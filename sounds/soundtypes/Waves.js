class Waves {
	constructor() {
    }

    static getSine(i,freq,vol,cyklePos,buf0,buf1,pitch,sampleRate,lowPassFilterActive,sampleSize,balL,balR) {
        var tempCyklePos = cyklePos;
        var cykle = (freq*pitch) / sampleRate;
        if(!(lowPassFilterActive == null || lowPassFilterActive ==undefined) && lowPassFilterActive == true) {
            // tempCyklePos = lowPassFilters(obj,j,cykle,null,tempCyklePos,obj.sampleRate);
        }
        
        buf0[i] += ((sampleSize * Math.sin((Math.PI *2) * tempCyklePos))*0.001)*vol*balL;
        buf1[i] += ((sampleSize * Math.sin((Math.PI *2) * tempCyklePos))*0.001)*vol*balR;
        cyklePos += cykle;
        if (cyklePos > 1)
            cyklePos -= 1;
        return {cyklePos:cyklePos};
        
    }
    static getSquare(obj,j,i, volCalc) {
        var tempCyklePos = obj.clips[j].cyklePos;
        var cykle = (obj.clips[j].freq*obj.clips[j].trackPitch) / obj.sampleRateDef;
        if(!(obj.clips[j].lowPassFilterActive == null || obj.clips[j].lowPassFilterActive ==undefined) && obj.clips[j].lowPassFilterActive == true) {
            tempCyklePos = lowPassFilters(obj,j,cykle,null,tempCyklePos,obj.sampleRateDef);
        }

        obj.tempBuf1[i] += ((obj.sampleSizeDef * Math.sign(Math.sin(2 * Math.PI * tempCyklePos)))*0.0001)*volCalc*obj.clips[j].balR;
        obj.tempBuf0[i] += ((obj.sampleSizeDef * Math.sign(Math.sin(2 * Math.PI * tempCyklePos)))*0.0001)*volCalc*obj.clips[j].balR;
        obj.clips[j].cyklePos += cykle;
        if (obj.clips[j].cyklePos > 1)
            obj.clips[j].cyklePos -= 1;
    }
    static getSaw(obj,j,i, volCalc) {
        var tempCyklePos = obj.clips[j].cyklePos;
        var cykle = obj.sampleRateDef / (obj.clips[j].freq*obj.clips[j].trackPitch);
        if(!(obj.clips[j].lowPassFilterActive == null || obj.clips[j].lowPassFilterActive ==undefined) && obj.clips[j].lowPassFilterActive == true) {
            tempCyklePos = lowPassFilters(obj,j,cykle,null,tempCyklePos,obj.sampleRateDef);
        }


        obj.tempBuf1[i] += (((2 * (tempCyklePos % cykle) / cykle - 1)*1000)*0.01)*volCalc*obj.clips[j].balR;
        obj.tempBuf0[i] += (((2 * (tempCyklePos % cykle) / cykle - 1)*1000)*0.01)*volCalc*obj.clips[j].balR;
        obj.clips[j].cyklePos++;
        if(obj.clips[j].cyklePos>=cykle) {
            obj.clips[j].cyklePos=0;
        }
    }
    static getTriangle(obj,j,i, volCalc) {
        var tempCyklePos = obj.clips[j].cyklePos;
        var cykle = obj.sampleRateDef / (obj.clips[j].freq*obj.clips[j].trackPitch);
        if(!(obj.clips[j].lowPassFilterActive == null || obj.clips[j].lowPassFilterActive ==undefined) && obj.clips[j].lowPassFilterActive == true) {
            tempCyklePos = lowPassFilters(obj,j,cykle,null,tempCyklePos,obj.sampleRateDef)
        }

        obj.tempBuf1[i] += (((2 * (tempCyklePos % cykle) / cykle - 1)*1000)*0.05)*volCalc*obj.clips[j].balR;
        obj.tempBuf0[i] += (((2 * (tempCyklePos % cykle) / cykle - 1)*1000)*0.05)*volCalc*obj.clips[j].balL;

        if(obj.clips[j].direction==0) {
            obj.clips[j].cyklePos++;
        }
        if(obj.clips[j].direction==1) {
            obj.clips[j].cyklePos--;
        }
        if(obj.clips[j].cyklePos>=cykle-1) {
            obj.clips[j].direction=1;
        }
        if(obj.clips[j].cyklePos<=0) {
            obj.clips[j].direction=0;
        }
    }

    static getWhiteNoise(obj,j,i, volCalc) {
        // limited (1.0 as example) variance and no means
        obj.tempBuf1[i] += this.gaussianWhiteNoiseWithVarianceAndMean(volCalc, obj.clips[j].whiteNoiseVariance,0);
        obj.tempBuf0[i] += this.gaussianWhiteNoiseWithVarianceAndMean(volCalc, obj.clips[j].whiteNoiseVariance, 0);
    }
    static gaussianWhiteNoiseWithVarianceAndMean(multiplying, variance, mean) {
		return this.whiteNoiseWithMeansAndVariance(variance, mean)*multiplying;
	}
	static whiteNoiseWithMeansAndVariance(variance, mean) {
        var randNr= this.randomGaussian();
        var response = randNr * Math.sqrt(variance) + mean;
		return response;
	}
    static randomGaussian() {
        var v = 25;
        var r = 0;
        for(var i = v; i > 0; i --){
            r += Math.random();
        }
        return r / v; 
    }

    static getPinkNoise(obj,j,i, volCalc) {
        // 20-20000 lowest-highest freq, quality is number of samples value is based on
        obj.tempBuf1[i] += this.calcPinkNoise(obj.clips[j].pinkNoiseQuality, obj.clips[j].pinkNoiseLowestFreq, obj.clips[j].pinkNoiseHighestFreq, volCalc);
        obj.tempBuf0[i] += this.calcPinkNoise(obj.clips[j].pinkNoiseQuality, obj.clips[j].pinkNoiseLowestFreq, obj.clips[j].pinkNoiseHighestFreq, volCalc);
    }
    static calcPinkNoise(quality, lowestFrequency, highestFrequency, volCalc)
	{
	    var d;
	    var lowestWavelength = highestFrequency / lowestFrequency;
	    for (var j = 0; j < quality; j++)
	    {
	        var wavelength = Math.pow(lowestWavelength, (j * 1.0) / quality)  * 44100 / highestFrequency;
	        var offset = Math.random() * Math.PI*2;     // Important offset is needed, as otherwise all the waves will be almost in phase, and this will ruin the effect!
	        d = Math.cos(i * Math.PI * 2 / wavelength + offset) / quality * volCalc;
	    }
	    return d;
	}
}