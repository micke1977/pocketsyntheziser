function playBuffer(clip) {
    ctx = new AudioContext();
    buffer = ctx.createBuffer(2, clip.chunkSize, clip.sampleRate);
    var buf0    = buffer.getChannelData(0);
    var buf1    = buffer.getChannelData(1);
    for (i = 0; i < clip.chnkSize; ++i) {
        buf0[i] = 0;
        buf1[i] = 0;
    }

    clip.getChunk(buf0,buf1);
               
    node = ctx.createBufferSource(0);
    node.buffer = buffer;
    node.connect(ctx.destination);
    node.onended = function() {
        if(node!=null) {
            node.stop(0);
            node = null;
        }
        buffer = null;
        ctx = null;
    }
    node.start();
}
function clipNote(oct,freq) {
    var clip;
    var freqCalculated =  0;

    if(freq == "c") {
        freqCalculated = 261.63
    }
    if(freq == "c#") {
        freqCalculated = 277.18
    }
    if(freq == "d") {
        freqCalculated = 293.66
    }
    if(freq == "d#") {
        freqCalculated = 311.13
    }
    if(freq == "e") {
        freqCalculated = 329.63
    }
    if(freq == "f") {
        freqCalculated = 349.23
    }
    if(freq == "f#") {
        freqCalculated = 369.99
    }
    if(freq == "g") {
        freqCalculated = 392.00
    }
    if(freq == "g#") {
        freqCalculated = 415.30
    }
    if(freq == "a") {
        freqCalculated = 440.00
    }
    if(freq == "a#") {
        freqCalculated = 466.16
    }
    if(freq == "b") {
        freqCalculated =493.88
    }
    

    if(oct == 1) {
        freqCalculated = freqCalculated * 2;
    }

    clip = new SoundFxSineTone(freqCalculated);
    this.playBuffer(clip);
}
/*
var timerSpeed = 100;
var timerIsRunning = true;
var timerIndex = 0;
*/
function timerFunction() {
    if(timerIndex == -1) {}
    else 
    {
        document.getElementById('timer_ctl_id_j_'+timerIndex).classList.remove('timer_selected')
        document.getElementById('timer_ctl_id_j_'+timerIndex).classList.add('timer')
    }
    timerIndex++;
    if(timerIndex>=nrOfSteps) {
        timerIndex = 0;
    }
    document.getElementById('timer_ctl_id_j_'+timerIndex).classList.add('timer_selected')
    if(timerIsRunning) {
        setTimeout(timerFunction, timerSpeed);
    }

    for(var i=0;i<nrOftones;i++) {
        if(tonePos[i][timerIndex]!=false) {
           
            clipNote( notesAndOcts[i].oct, notesAndOcts[i].note);
        }
    }

}
function startTimer() {
    for(var i = 0;i<nrOfSteps;i++) {
        document.getElementById('timer_ctl_id_j_'+i).classList.remove('timer_selected')
        document.getElementById('timer_ctl_id_j_'+i).classList.add('timer')
    }
    timerIsRunning = true;
    timerIndex = -1;
    setTimeout(timerFunction, timerSpeed);
}
function stopTimer() {
    timerIsRunning = false;
}
function incrSpeed() {
    timerSpeed+=20;
    document.getElementById('timer_speed_id').textContent = timerSpeed;
}
function decrSpeed() {
    timerSpeed-=20;
    if(timerSpeed<20) {
        timerSpeed=20;
    }
    document.getElementById('timer_speed_id').textContent = timerSpeed;
}

