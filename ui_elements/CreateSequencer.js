
function createSequencers() {
    //i = tone, j = pos

    for(var i = 0;i<nrOftones;i++) {
        var tonePosRow = [];
        for(var j=0;j<nrOfSteps;j++) {
            tonePosRow.push(false);
        }
        tonePos.push(tonePosRow);
    }


     var scrollFrame = document.getElementById('scrollId')


    //_start_________________________________________________________
    var timerBtnRowDiv=document.createElement('div');
    timerBtnRowDiv.classList.add('row');

      //----------
    var startTimerBtn = document.createElement('div')
    startTimerBtn.textContent = 'START SEQUENCER';
    startTimerBtn.addEventListener("click", startTimer)
    startTimerBtn.classList.add('timer_start_btn')
    timerBtnRowDiv.appendChild(startTimerBtn);
      //----------

      //----------
    var stopTimerBtn = document.createElement('div')
    stopTimerBtn.textContent = 'STOP SEQUENCER';
    stopTimerBtn.addEventListener("click", stopTimer)
    stopTimerBtn.classList.add('timer_stop_btn')
    timerBtnRowDiv.appendChild(stopTimerBtn);
      //----------



      //----------
    var incrTimerBtn = document.createElement('div')
    incrTimerBtn.textContent = '+20';
    incrTimerBtn.addEventListener("click", incrSpeed)
    incrTimerBtn.classList.add('timer_incr_decr_btn','col-2')
    timerBtnRowDiv.appendChild(incrTimerBtn);
      //----------

    //----------
    var timerSpeedTxt = document.createElement('div')
    timerSpeedTxt.textContent = timerSpeed;
    timerSpeedTxt.setAttribute('id','timer_speed_id');
    timerSpeedTxt.classList.add('timer_speed_txt','col-3')
    timerBtnRowDiv.appendChild(timerSpeedTxt);
      //----------

      //----------
    var decrTimerBtn = document.createElement('div')
    decrTimerBtn.textContent = '-20';
    decrTimerBtn.addEventListener("click", decrSpeed)
    decrTimerBtn.classList.add('timer_incr_decr_btn','col-2')
    timerBtnRowDiv.appendChild(decrTimerBtn);
      //----------

      //----------
    var downloadBtn = document.createElement('div')
    downloadBtn.textContent = 'Download as text';
    downloadBtn.addEventListener("click", createAndDownloadFile)
    downloadBtn.classList.add('download_btn','col-5')
    timerBtnRowDiv.appendChild(downloadBtn);
        //----------

      //----------
    var clearAllBtn = document.createElement('div')
    clearAllBtn.textContent = 'Clear all content';
    clearAllBtn.addEventListener("click", clearAll)
    clearAllBtn.classList.add('timer_stop_btn','col-5')
    timerBtnRowDiv.appendChild(clearAllBtn);
        //----------


     scrollFrame.appendChild(timerBtnRowDiv);

     scrollFrame.appendChild(document.createElement('br'));
    //_end___________________________________________________________



     for(var i = 0;i<1;i++) {
      var timerRowDiv =  document.createElement('div');
      timerRowDiv.setAttribute('id','timer_id_i_'+i+'_j_'+j);
      timerRowDiv.classList.add('row');
      for(var j=0;j<nrOfSteps;j++) {
          var timerCol = document.createElement('div');
          timerCol.textContent = j;
          timerCol.setAttribute('id','timer_ctl_id_j_'+j);
          timerCol.classList.add('timer','col-1');      
          timerRowDiv.appendChild(timerCol);
      }
      scrollFrame.appendChild(timerRowDiv);

      scrollFrame.appendChild(document.createElement('br'));
      
   }

     for(var i = 0;i<nrOftones;i++) {
        var sequencerRowDiv =  document.createElement('div');
        sequencerRowDiv.setAttribute('id','sequencer_id_i_'+i+'_j_'+j);
        sequencerRowDiv.classList.add('row');
        for(var j=0;j<nrOfSteps;j++) {
            var sequencerCol = document.createElement('div');
            sequencerCol.textContent = 'p:'+j+' tn:'+notesAndOcts[i].note+' o:'+notesAndOcts[i].oct;
            sequencerCol.setAttribute('id','sequencer_ctl_id_i_'+i+'_j_'+j);
            sequencerCol.classList.add('sequencer','col-1');
            sequencerCol.addEventListener("click", callback(i,j))
        
            sequencerRowDiv.appendChild(sequencerCol);
        }
        scrollFrame.appendChild(sequencerRowDiv);

        scrollFrame.appendChild(document.createElement('br'));
        
     }
}

function callback(i,j) {
    return function() {
      tonePos[i][j] = !tonePos[i][j];
      var sec_ctl = document.getElementById(('sequencer_ctl_id_i_'+i+'_j_'+j))

      if(tonePos[i][j]) {
        sec_ctl.classList.remove("sequencer");
        sec_ctl.classList.add("sequencer_selected");
      } else {
        sec_ctl.classList.remove("sequencer_selected");
        sec_ctl.classList.add("sequencer");
      }
    }
  }
  