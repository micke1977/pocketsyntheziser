var tonePos = [];
var timerSpeed = 100;
var timerIsRunning = true;
var timerIndex = 0;
var nrOfSteps = 256;
var nrOftones = 24;
var notesAndOcts = [
    {note:'c',oct:0},
    {note:'c#',oct:0},
    {note:'d',oct:0},
    {note:'d#',oct:0},
    {note:'e',oct:0},
    {note:'f',oct:0},
    {note:'f#',oct:0},
    {note:'g',oct:0},
    {note:'g#',oct:0},
    {note:'a',oct:0},
    {note:'a#',oct:0},
    {note:'b',oct:0},
    {note:'c',oct:1},
    {note:'c#',oct:1},
    {note:'d',oct:1},
    {note:'d#',oct:1},
    {note:'e',oct:1},
    {note:'f',oct:1},
    {note:'f#',oct:1},
    {note:'g',oct:1},
    {note:'g#',oct:1},
    {note:'a',oct:1},
    {note:'a#',oct:1},
    {note:'b',oct:1},
    
]

function createAndDownloadFile() {
    var content = 'Song:\n';
    for(var i = 0;i<tonePos.length;i++) {
        for( var j = 0;j<tonePos[i].length;j++) {
            if(tonePos[i][j] == true) {
                content+= "pos:"+j+" tone:"+notesAndOcts[i].note+" oct:"+notesAndOcts[i].oct+"\n";
            }
            
        }
    }

    var file = new File(["\ufeff"+content], 'songfile_'+Math.random().toFixed(3)+'.txt', {type: "text/plain:charset=UTF-8"});

    url = window.URL.createObjectURL(file);

    var a = document.createElement("a");
    a.style = "display: none";
    a.href = url;
    a.download = file.name;
    a.click();
    window.URL.revokeObjectURL(url);
}

function clearAll() {
    for(var i = 0;i<tonePos.length;i++) {
        for( var j = 0;j<tonePos[i].length;j++) {
            tonePos[i][j] = false;
            document.getElementById('sequencer_ctl_id_i_'+i+'_j_'+j).classList.remove("sequencer_selected");
            document.getElementById('sequencer_ctl_id_i_'+i+'_j_'+j).classList.add("sequencer");
        }
    }
}
